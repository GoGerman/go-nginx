package main

import (
	"gitlab.com/GoGerman/go-nginx/cmd/app"
)

func main() {
	app.Run()
}
