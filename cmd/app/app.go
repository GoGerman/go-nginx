package app

import (
	"gitlab.com/GoGerman/go-nginx/internal/modules/handlers"
	"gitlab.com/GoGerman/go-nginx/internal/modules/repository"
	"gitlab.com/GoGerman/go-nginx/internal/modules/service"
	"gitlab.com/GoGerman/go-nginx/internal/router"
)

func Run() {
	repo := repository.NewVacancyRepository()
	serv := service.NewVacancyService(repo)
	contr := handlers.NewVacancyController(serv)
	router.RunServer(contr)
}
