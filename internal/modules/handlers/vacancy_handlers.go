package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/GoGerman/go-nginx/internal/models"
	"net/http"
	"strconv"
)

type InterNginx interface {
	Search(lang string) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacancyController struct {
	Data InterNginx
}

func NewVacancyController(serv InterNginx) *VacancyController {
	return &VacancyController{Data: serv}
}

func (v *VacancyController) List(w http.ResponseWriter, r *http.Request) {
	list, err := v.Data.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(list)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) Get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	num, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	data, err := v.Data.GetById(num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) Delete(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	num, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = v.Data.Delete(num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
}

func (v *VacancyController) Search(w http.ResponseWriter, r *http.Request) {
	lang := chi.URLParam(r, "lang")

	err := v.Data.Search(lang)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(err)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
