package service

import (
	"gitlab.com/GoGerman/go-nginx/internal/models"
)

type InterNginx interface {
	Search(lang string) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacancyService struct {
	Data InterNginx
}

func NewVacancyService(repo InterNginx) InterNginx {
	return &VacancyService{Data: repo}
}

func (v *VacancyService) Search(lang string) error {
	return v.Data.Search(lang)
}

func (v *VacancyService) GetById(id int) (models.Vacancy, error) {
	return v.Data.GetById(id)
}

func (v *VacancyService) GetList() ([]models.Vacancy, error) {
	return v.Data.GetList()
}

func (v *VacancyService) Delete(id int) error {
	return v.Data.Delete(id)
}
